import pytest
from app import app as flask_app

'''
> python -m pytest

3 kinds of pytest parameters:

functional/test_api.py
@pytest.mark.validity_test
    > python -m pytest -v -m validity_test

functional/test_api.py
@pytest.mark.range_test
    > python -m pytest -v -m range_test

unit/test_model.py
@pytest.mark.consistency_test
    > python -m pytest -v -m consistency_test

'''
@pytest.fixture
def app():
    yield flask_app





@pytest.fixture
def client(app):
    print(app.static_url_path)
    return app.test_client()


