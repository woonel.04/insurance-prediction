'''
TEST MODEL

This Pytest file is to test the model ONLY 

add_entry()
get_entries()
get_entry()
delete()


This Pytest file also tests consistency for the ai_model
'''

# imports
import pytest
import flask
import datetime


# import file classes from app folder
from app import Preprocessor , CRUD, models

# import ai model 
from app import ai_model , app





# the following SHOULD PASS all tests
unexpected_failure_list = [
    #[age, sex     , bmi  , children, smoker, region     , charges ], 
    [ 32 , 'male'  , 28.88, 0       , 'no'  , 'northwest', 3866.86 ], #BMI: Float , Charges: Float
    [ 63 , 'female', 24   , 0       , 'no'  , 'northeast', 14451.83], # BMI : Integer, Charges : Float
    [ 64 , 'male'  , 42   , 5       , 'yes' , 'southwest', 32389   ], # BMI : Integer, Charges : Integer
    [50  , 'male'  , 18.35, 2       , 'no'  , 'northwest', 30284.64294],
    [18  , 'female', 38.17, 0       , 'no'  , 'southeast', 1631.6683],

]


'''
VALIDITY TESTING / UNEXPECTED FAILURE TESTING
    testing model
'''


# -----------------------------------------------------------------
#    CREATE 
# -----------------------------------------------------------------
global validity_model_test_add_id
# list to store all the added ids
validity_model_test_add_id = []

@pytest.mark.order(1) # create first before anything else
@pytest.mark.parametrize("entrylist", unexpected_failure_list)
@pytest.mark.validity_model_test

def test_model_validity_add(entrylist,):
    with app.app_context():
        
        #prepare the data into a dictionary
        col_names = ["age", "sex", "bmi", "children", "smoker", "region", "prediction"]
        data_body =  dict(zip(col_names, entrylist))

        data_body["predicted_on"] = datetime.datetime.utcnow()
        
        new_entry = models.Entry(**data_body)
        

        CRUD.CRUD.add_entry(new_entry)

        validity_model_test_add_id.append(new_entry.id)
        for key, value in data_body.items():
            assert getattr(new_entry, key) == value



# -----------------------------------------------------------------
#    GET ALL
# -----------------------------------------------------------------

@pytest.mark.validity_model_test
def test_model_validity_get_all():
    '''
    GET ALL 
        get all results available in table 'predictions'
    '''
    with app.app_context():
        assert CRUD.CRUD.get_entries()



# -----------------------------------------------------------------
#    GET ONE 
# -----------------------------------------------------------------
@pytest.mark.parametrize("index",range(len(unexpected_failure_list)))
@pytest.mark.validity_model_test
def test_model_validity_get_one( index, ):
    '''
    Create a test client and see if flask is running
    '''
    with app.app_context():
        # if id doesnt exist, it will throw 
        #     RuntimeError: Working outside of request context
        assert CRUD.CRUD.get_entry(validity_model_test_add_id[index]) 








# -----------------------------------------------------
#     DELETE
# -----------------------------------------------------
@pytest.mark.validity_model_test
@pytest.mark.parametrize("index",range(len(unexpected_failure_list)))
def test_model_validity_delete_one( index):
    with app.app_context():
        # if id doesnt exist, it will throw 
        #     RuntimeError: Working outside of request context

        assert CRUD.CRUD.remove_entry(validity_model_test_add_id[index]) 




# -----------------------------------------------------------------
#    CREATE & DELETE ALL
# -----------------------------------------------------------------

@pytest.mark.parametrize("entrylist", unexpected_failure_list)
@pytest.mark.validity_model_test

def test_model_validity_delete_all(entrylist,):
    with app.app_context():
        '''
        ADD DATA INTO DICTIONARY
        '''
        #prepare the data into a dictionary
        col_names = ["age", "sex", "bmi", "children", "smoker", "region", "prediction"]
        data_body =  dict(zip(col_names, entrylist))

        data_body["predicted_on"] = datetime.datetime.utcnow()
        
        new_entry = models.Entry(**data_body)

        CRUD.CRUD.add_entry(new_entry)

        for key, value in data_body.items():
            assert getattr(new_entry, key) == value


        '''
        DELETE ALL
        '''
        response_code = CRUD.CRUD.remove_entries()
        assert response_code == 202





'''
VALIDATION EXPECTED FAILURE TEST
'''



# the following SHOULD FAIL all tests
expected_failure_list = [
    #[age , sex    , bmi  , children, smoker, region     , charges ], 
    [ 32.9, "male" , 28.88, 0       , 'no'  , 'northwest', 3866.86], # float age
    [ 32  , 1      , 28.88, 0       , 'no'  , 'northwest', 3866.86], # numerical sex
    [ 32  , "male" , 28.88, 0.2     , 'no'  , 'northwest', 3866.86], # float children
    [ 32  , "male" , 28.88, 0       , 0     , 'northwest', 3866.86], # numerical smoker
    [ 32  , "male" , 28.88, 0       , 'no'  , 0          , 3866.86], # numerical region
]


# -----------------------------------------------------------------
#    CREATE 
# -----------------------------------------------------------------
@pytest.mark.validity_model_test
@pytest.mark.parametrize("entrylist", expected_failure_list)
@pytest.mark.xfail

def test_model_validity_expected_failure(entrylist,):
    
        # same code as test_model_validity_add(entrylist), except no appending to list
        #prepare the data into a dictionary
        col_names = ["age", "sex", "bmi", "children", "smoker", "region", "prediction"]
        data_body =  dict(zip(col_names, entrylist))

        data_body["predicted_on"] = datetime.datetime.utcnow()
        
        new_entry = models.Entry(**data_body)

        CRUD.CRUD.add_entry(new_entry)
        for key, value in data_body.items():
            assert getattr(new_entry, key) == value





'''
CONSISTENCY TESTING

'''


'''
TEST AI MODEL
'''

# Test get API
@pytest.mark.consistency_model_test
@pytest.mark.parametrize("entrylist",unexpected_failure_list)
def test_ai_model(entrylist, capsys):

    with capsys.disabled():

        # data preprocessing
        X, _, _ = Preprocessor.Preprocessor.db_to_pd(
            entrylist, 
            order_cols = ['age', 'sex', 'bmi', 'children', 'smoker', 'region', 'prediction'],
            y_col = ['prediction'],
        )

        
        X = Preprocessor.Preprocessor.preprocessor(
            X,  # just want the features, not the last column
        )
        
        result = ai_model.predict(X)[0]

        # test that it is a positive number and its not a million (crazy number)
        assert result > 0 and result < 10 **6




@pytest.mark.consistency_model_test
@pytest.mark.parametrize("entrylist",unexpected_failure_list)

def test_consistency(entrylist, capsys):

    with capsys.disabled():
        '''
        Just a test to make sure the model is working 
        '''


        # data preprocessing
        X_1, _, _ = Preprocessor.Preprocessor.db_to_pd(
            entrylist, 
            order_cols = ['age', 'sex', 'bmi', 'children', 'smoker', 'region', 'prediction'],
            y_col= ['prediction'],
        )

        
        X_1 = Preprocessor.Preprocessor.preprocessor(
            X_1,  # just want the features, not the last column
        )
        
        result_1 = ai_model.predict(X_1)[0]

        assert result_1 


        # data preprocessing
        X_2, _, _= Preprocessor.Preprocessor.db_to_pd(
            entrylist,
            order_cols = ['age', 'sex', 'bmi', 'children', 'smoker', 'region', 'prediction'],
            y_col = ['prediction'],
            )

        
        X_2 = Preprocessor.Preprocessor.preprocessor(
            X_2,  # just want the features, not the last column
        )
        
        
        result_2 = ai_model.predict(X_2)[0]

        assert result_1 == result_2


        # data preprocessing
        X_3, _, _= Preprocessor.Preprocessor.db_to_pd(
            entrylist,
            order_cols = ['age', 'sex', 'bmi', 'children', 'smoker', 'region', 'prediction'],
            y_col = ['prediction'],
            )

        
        X_3 = Preprocessor.Preprocessor.preprocessor(
            X_3,  # just want the features, not the last column
        )
        
        
        result_3 = ai_model.predict(X_3)[0]

        # test that it is a positive number and its not a million (crazy number)
        assert result_1 == result_3
        assert result_2 == result_3
