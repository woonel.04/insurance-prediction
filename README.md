# Insurance Prediction

Web Development project using Python Flask and SQLite. This project also features a simple data analysis project for the regression model. 

## Introduction to Project

- Model Building
    - Regression Model to predict the insurance charge of a person based on given values

- Web Application building
    - Using Python Flask and SQLite
    
- Deployment 
    - to Render at render.com
