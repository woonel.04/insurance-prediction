'''
Preprocessor
    !! NOT REPLICABLE ON ANY OTHER DATASET !!
        hard coded for insurance.csv dataset and GradientBoostingRegressor
    
sorry for poor naming convention
'''

import pandas as pd 

class Preprocessor:
    '''
    FROM DATABASE TO PANDAS
    '''

    def db_to_pd(
        data, # data  (features and target column) 
        order_cols = ['age', 'sex', 'bmi', 'children', 'smoker', 'region', 'prediction', 'predicted_on'],
        y_col = None,
        ):

        '''
        ARGS
            data
                type : list/ numpy array 

            order
                type : list/ numpy array 
        '''
        df = pd.DataFrame(data).transpose()
        df.columns = order_cols
        
        if y_col is None: 
            return df

        else: 
            return (
                df.drop(columns = y_col, axis = 1),  # X values
                df[y_col],   # y values
                df, # dataframe
            )



    def preprocessor(
        X, 
        order_cols = ["age", "bmi", "children", "sex", "smoker", "region",]
        ):

        '''
        STEPS 
            sort columns by custom list 
            
            one hot encodes categorical data
            
            change column name in accordance to one hot encoder
                e.g. : 'sex' to 'sex_male'


        EXPECTED COLUMNS
            age	| bmi |	children |	sex_male |	smoker_yes |	region_northwest |	 region_southeast |	region_southwest

        Preprocessor
            - one hot encode categorical data
        '''
        if  isinstance(X, dict):
            X = pd.DataFrame(X)

        if isinstance(X, pd.DataFrame):
            df = pd.DataFrame()
                

            # order the pandas dataframe nicely by expected columns
            # age	| bmi |	children |	sex |	smoker |	region
            for col_name in order_cols: 
                df[col_name] = X[col_name]
                

        else : # type numpy, probably, or a list
            df = pd.DataFrame(X).transpose()
            df.columns = order_cols
            pass

        

        
        

        #------------------------------------------------------------
        #     REPLACE 'sex' COLUMN
        #------------------------------------------------------------
        df['sex'] = df['sex'].replace(to_replace='male', value=1)
        df['sex'] = df['sex'].replace(to_replace='female', value=0)
        
        #------------------------------------------------------------
        #     REPLACE 'smoker' COLUMN
        #------------------------------------------------------------
        df['smoker'] = df['smoker'].astype(str)
        df['smoker'] = df['smoker'].replace(to_replace='yes', value=1)
        df['smoker'] = df['smoker'].replace(to_replace='no', value=0)


        #------------------------------------------------------------
        #     REPLACE 'region' COLUMN
        #------------------------------------------------------------
        regions = ['northwest', 'southeast', 'southwest']
        for region in regions:
            df[f"region_{region}"] = 0
            if df['region'][0] == region:
                df[f"region_{region}"] = 1

        # deleting region
        del df['region']

        
        # rename columns so GradientBoostingRegressor doesn't throw me an error
        '''
        age | bmi | children | sex_male | smoker_yes | region_northwest | region_southeast | region_southwest
        '''

        df = df.rename(columns={"sex": "sex_male", "smoker": "smoker_yes"})



        # print(f"\nX data:\n\n{df}\n\n")
        return df