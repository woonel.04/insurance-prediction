#####################################################
############## COMMITS TO FORMS #####################
#####################################################


import flask_wtf
import wtforms





class PredictionForm(flask_wtf.FlaskForm):
    '''
    age
        IntegerField()

        Validation:
            InputRequired()
            NumberRange(min = 18, max = 64)

    sex
        RadioField(),

        Choices:
            ["male", "female"]

        Validation:
            InputRequired(),

    BMI
        FloatField()

        Validation:
            InputRequired(), 
            NumberRange(15.96,53.13)

    Number Of Children
        IntegerField()

        Validation:
            InputRequired()
            NumberRange(min = 0, max = 5)

    smoker
        StringField(),

        Validation:
            InputRequired(),
            AnyOf( values=["yes", "no"])        
            
    region
        SelectField(),

        Validation:
            InputRequired(),
            AnyOf( values=["northwest", "northeast", "southwest", "southeast"])

    '''

    age = wtforms.IntegerField(
        "Age", 
        validators = [
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(min = 18, max = 64, message = "Anything not between 18 and 64 would wield inaccurate results"),
        ]
    )


    sex = wtforms.RadioField(
        "Sex",
        choices = [("male", "Male"), ("female", "Female"), ], 
        validators = [
            wtforms.validators.InputRequired(),
        ]
    )


    bmi = wtforms.FloatField(
        "BMI", 
        validators = [
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(min = 15.96, max = 53.13, message = "Anything not between 15.96 and 53.13 would wield inaccurate results"),
        ]
    )

    children = wtforms.IntegerField(
        "Number of Children", 
        validators = [
            wtforms.validators.InputRequired(),
            wtforms.validators.NumberRange(min = 0, max = 5, message = "Anything not between 0 and 5 would wield inaccurate results"),
        ]
    )


    smoker = wtforms.BooleanField(
        "Smoker", 
        # choices = ["yes", "no"],
        # coerce=str,
        validators = [
            # wtforms.validators.InputRequired(),
            # wtforms.validators.AnyOf( values=["yes", "no"]),
        ]
    )

    region = wtforms.SelectMultipleField(
        "Region", 
        coerce=str,
        choices = ["northwest", "northeast", "southwest", "southeast"], 
        validators = [
            wtforms.validators.InputRequired(),
        ]
    )


    submit = wtforms.SubmitField("Calculate!")









class LoginForm(flask_wtf.FlaskForm): 
    '''
    VALIDATION
        InputRequired
        Email
        AnyOf
    '''
    email = wtforms.StringField(
        "Email", 
        validators = [
            # wtforms.validators.Regexp('\w+', message="Valid email please"),
            wtforms.validators.InputRequired(),
            wtforms.validators.AnyOf( values=["kohqichang@mail.com"] , message = "Email Address Doesn't Exist"),
            wtforms.validators.Email(),
        ]
    )

    
    password = wtforms.StringField(
        "Password", 
        widget= wtforms.widgets.PasswordInput(hide_value=False),
        validators = [
            wtforms.validators.InputRequired(),
            wtforms.validators.AnyOf( values=["password1"] , message = "Wrong Password"),
        ]
    )
    submit = wtforms.SubmitField("Login")