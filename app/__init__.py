'''
GLOBAL VARIABLES
app
ai_model
database

'''

#-------------------------------------------------------
#    AI MODEL PICKLE FILE
#-------------------------------------------------------
import pickle

## Get the file path first
import os 
directory_path = os.path.dirname(os.path.abspath(__file__))

model_pkl = "static/model/model.pkl"
relative_path = os.path.join(directory_path, model_pkl)

# absolute file path to model file
absolute_file_name = os.path.abspath(relative_path)
print(absolute_file_name)
# Load from file


with open(absolute_file_name, 'rb') as f:
    '''
    !!!! SETTING GLOBAL VARIABLE !!!
    '''
    ai_model = pickle.load(f)




import flask 
#create the Flask app
app = flask.Flask(__name__)
# load configuration from config.cfg
app.config.from_pyfile('config.cfg')

#-------------------------------------------------------
#    DATABASE
#-------------------------------------------------------
import flask_sqlalchemy 
# instantiate SQLAlchemy to handle database process
database = flask_sqlalchemy.SQLAlchemy()

#create the flask.Flask app
app = flask.Flask(__name__)
# load configuration from config.cfg
app.config.from_pyfile('config.cfg')
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///database.db"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# new method for SQLAlchemy version 3 onwards
with app.app_context():
    database.init_app(app)

    from .models import Entry
    database.create_all()
    database.session.commit()
    print('CREATED - Database!')


#run the file routes.py
from app import routes